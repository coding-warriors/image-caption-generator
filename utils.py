from PIL import Image
import numpy as np
from keras.models import Model, load_model
from pickle import dump ,load
from keras.applications.xception import Xception
from keras.preprocessing.sequence import pad_sequences


def extract_features(filename, model):
        try:
            image = Image.open(filename)
        except:
            print("image cannot be opened")
        image = image.resize((299,299))
        image = np.array(image)
        # 4 channels to 3 channels
        if image.shape[2] == 4:
            image = image[..., :3]
        image = np.expand_dims(image, axis=0)
        image = image/127.5
        image = image - 1.0
        feature = model.predict(image)
        return feature
def word_for_id(integer, tokens):
     for word, index in tokens.word_index.items():
         if index == integer:
             return word
     return None
def gen_desc(model, tokenizer, photo, max_length):
    in_text = 'start'
    for i in range(max_length):
        sequence = tokenizer.texts_to_sequences([in_text])[0]
        sequence = pad_sequences([sequence], maxlen=max_length)
        pred = model.predict([photo,sequence], verbose=0)
        pred = np.argmax(pred)
        word = word_for_id(pred, tokenizer)
        if word is None:
            break
        in_text += ' ' + word
        if word == 'end':
            break
    return in_text

def main(img_path:str):
    max_length = 34
    tokens = load(open("tokens.p","rb"))
    model = load_model('/content/drive/MyDrive/model_8.h5')
    xception_model = Xception(include_top=False, pooling="avg")
    photo = extract_features(img_path, xception_model)
    img = Image.open(img_path)
    description = gen_desc(model, tokens, photo, max_length)
    return description

