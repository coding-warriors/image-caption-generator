from PIL import Image
import streamlit as st
from utils import *

st.title("Image Caption Generator")
st.image(st.file_uploader("Choose an image...", type="jpg",width = 500)

if(st.button("Click here for caption")):
  img_path = '/content/drive/MyDrive/Flickr Dataset/Flicker8k_Dataset_Image/99679241_adc853a5c0.jpg'
  result = main(img_path)
  st.text("The Image Caption is {}.".format(result.strip('start').strip('end').strip()))
